package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {
	var length int
	var err error

	if len(os.Args) < 2 {
		length = 10
	} else {
		length, err = strconv.Atoi(os.Args[1])
		if err != nil {
			length = 10
		}
	}

	_, _ = fmt.Fprintln(os.Stdout, generatePassword(length))
}

func generatePassword(length int) string {
	const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	const numbers = "0123456789"
	const special = "!@#$%^&*()_-=+"
	const symbols = letters + numbers + special

	// If using Go before version 1.20, create a dedicated rand source.
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	password := make([]byte, length)
	for i := range password {
		b := symbols[r.Intn(len(symbols))]
		password[i] = b
	}
	return string(password)
}
